<?php get_header(); ?>

<div class="main" role="main">

  <!-- Page Header -->
  <div class="page themes-page">
    <section class="container">
      <div class="page-header">
        <div class="section-block">
          <h1 class="section-title">Themes</h1>
          <span class="sub-title">Choose a theme to get started</span>
        </div>
        
        <div class="filter-themes">
          <button class="filter-it" id="toggle">Filter Themes <i class="caret"></i></button>
            <?php 
              function return_menu_index() {
                  $taxonomies = array(
                    'categories',
                  );

                  $args = array(
                    'orderby'           => 'name', 
                    'order'             => 'ASC',
                    'hide_empty'        => false, 
                    'fields'            => 'all',
                    'parent'            => 0,
                    'hierarchical'      => true,
                    'child_of'          => 0,
                    'pad_counts'        => false,
                    'cache_domain'      => 'core'    
                  );

                  $menus = get_terms($taxonomies, $args);

                  $return .= '<ul class="filter-items">'; 
                    
                    $return .= sprintf(
                      '<li><a class="anim-black" href="/themes">All</a></li>',       
                      $menu->name
                    );
                    foreach ( $menus as $menu ) {

                      $submenus = get_terms($taxonomies, array(
                        'parent'   => $menu->term_id,
                        'hide_empty' => false
                      ));

                      if ( !empty($submenus) ) {
                        $has = 'has-children';
                      }
                      else {
                        $has = '';
                      }

                      $permalink = get_term_link($menu);

                      // return menu items
                      $return .= sprintf(
                        '<li class="'.$has.'"><a class="anim-black" href="'.$permalink.'">%1$s</a>',       
                        $menu->name
                      );

                        if ( !empty($submenus) ) {

                          $return .= '<i class="extend"></i>';

                          $return .= '<ul class="submenu">';

                          foreach ( $submenus as $submenu ) {

                            $permalink = get_term_link($submenu); 

                            //return sub menu items
                            $return .= sprintf(
                              '<li><a href="'.$permalink.'">%1$s</a>',       
                              $submenu->name
                            );

                            $return .= '</li>'; //end subterms li
                          }            

                          $return .= '</ul>'; //end subterms ul

                        }

                      $return .= '</li>'; //end terms li
                    } //end foreach term

                  $return .= '</ul>';

                return $return;
              }

              echo return_menu_index();
            ?>
        </div>
      </div>
      <div class="themes">
        <!-- Filter Theme Items -->
        <form class="controls" id="filters">
          <fieldset>
            <button class="filter-btn" data-filter=".free">Free</button>
            <span class="sep">/</span>
            <button class="filter-btn" data-filter=".premium">Premium</button>
          </fieldset>
        </form>
        <!-- Theme Items -->
        <div class="items">
        <div class="no-results">No items were found matching the selected filters.</div>
        <?php 
          $params = array( 
            'orderby' => 'menu_order',
            'limit' => -1
          );
          $themes = pods( 'themes', $params); 
        ?>
          
        <?php if ( $themes->total() > 0 ) { ?>
          <?php while ($themes->fetch() ) { ?>
          <?php 
            //PODS Fields
  
            $id         = $themes->field('id');
            $slug       = $themes->field('slug');
            $name       = $themes->field('theme_name');
            $info       = $themes->field('theme_info');
            $demo       = $themes->field('demo_link');
            $guide      = $themes->field('setup_guide');
            $official   = $themes->field('theme_link');
            $file       = wp_get_attachment_url( $themes->field('theme_file.ID') );
  
            //ACF (Advanced Custom Fields) Fields
            $imgID    = get_field('featured_image', $id);
            $desktop   = "fp_img_1920";
            $tablet    = "fp_img_1024";
            $mobile    = "fp_img_627";

            $desktop_url   = wp_get_attachment_image_src( $imgID, $desktop );
            $tablet_url    = wp_get_attachment_image_src( $imgID, $tablet );
            $mobile_url    = wp_get_attachment_image_src( $imgID, $mobile );
            
            //Get related terms
            $terms = get_the_terms( $id, 'type' ); 
  
            $i = 0;
            $i < $count;
          ?>
          <!-- Theme Single Item -->
          <article class="item <?php foreach ( $terms as $term ) : $i++ ?><?php echo $term->slug; ?><?php if ($i < ($count - 1)) {echo ', ';} ?><?php endforeach; ?>" style="display: none;">
            <div class="inner">
              <a class="modal-reveal ajax-<?php echo $id; ?>" rel="<?php echo $id; ?>" href="<?php echo esc_url( get_permalink($id) ); ?>">
                <span class="dots"></span>
                <picture>
                  <img itemprop="image" src="<?php if ($mobile_url){ echo $mobile_url[0]; } ?>" alt="<?php echo $name; ?>" title="<?php echo $name; ?>" />
                </picture>
                <span class="modal-find"><i class="icon-search" aria-hidden="true"></i></span>
              </a>
            </div>
            <h1 class="title"><?php echo $name ?></h1>
          </article>
          <?php } ?>
        <?php } ?>
        </div>
        <!-- Pagination -->
        <div class="paginate">
          <div class="pagination"></div>
        </div>
      </div>
    </section>
  </div>
  
</div>
<?php get_footer(); ?>