<?php
/*
Template name: Home Page
*/

get_header(); ?>

<div class="main" role="main">
  <?php 

    //ACF (Advanced Custom Fields) Fields
    $img    = get_field('home_image');
  
    $title = get_field('home_title');
    $btn = get_field('home_button_text');
    $link = get_field('home_button_link');

  ?>
  <div class="page page-404">
    <div class="container">
      <section>
        <h1>We couldn't find it...</h1>
        <p>Looks like the page you are looking for no longer exists. <br>Please double-check the URL or go to our homepage.</p>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn green medium">Go to Homepage</a>
      </section>
      <picture class="error-image">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/404_penguin.svg" />
      </picture>
    </div>
  </div>
  
</div>
<?php get_footer(); ?>