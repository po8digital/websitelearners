var toggleClose = $('.close');
var menuWrapper = $('.main-nav');
var bodyControl = $('body');
var mouse_is_inside = false;

$('.navigate').click( function() {
  
  bodyControl.removeClass("active");
  menuWrapper.addClass("active");
  
  bodyControl.addClass("active");
  return false;
});

toggleClose.click( function() {
  
  bodyControl.removeClass("active");
  menuWrapper.removeClass("active");
  return false;
});

jQuery(document).ready(function($) {

  $('#toggle').on("click", function(e) {
    $(this).toggleClass('on');
    e.preventDefault();
  });
  
  $('.has-download').on("click", function(e) {
    $(this).parent().toggleClass('on');
    e.preventDefault();
  });

  $('li.menu-item-has-children > a').on("click", function(e) {
    $(this).toggleClass('on');
    e.preventDefault();
  });
  
  $(document).on("click", function(e) {
    if ($(e.target).is("li.menu-item-has-children > a, ul.submenu") === false) {
      $("li.menu-item-has-children > a").removeClass("on");
    }
    if ($(e.target).is(".filter-it, ul.filter-items") === false) {
      $(".filter-it").removeClass("on");
    }
    if ($(e.target).is(".has-download, .operator") === false) {
      $(".viewer").removeClass("on");
    }
  });
  
  $('input:text#Search').each(function(i,el) {
    if (!el.value || el.value == '') {
      el.placeholder = 'example.com';
      /* or:
      el.placeholder = $('label[for=' + el.id + ']').text();
      */
    }
  });
  
});


jQuery(document).ready(function(){
  
  jQuery(function($){
    $.ajaxSetup({
      cache:false,
      success: function() {
        
      },
      complete: function() {
        
        $(document).ready(function() {  
          
          $('.modal-carousel-main').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            fade: true,
            infinite: true,
            lazyLoad: 'ondemand',
            initOnLoad: true,
            nextSlidesToPreload: 2,
            draggable: false,
            asNavFor: '.modal-carousel-vertical'
          });
          
          $('.modal-carousel-vertical').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: '.modal-carousel-outer',
            vertical: true,
            dots: false,
            arrows: false,
            infinite: true,
            centerMode: true,
            draggable: false,
            lazyLoad: 'ondemand',
            initOnLoad: true,
            nextSlidesToPreload: 6,
            focusOnSelect: true
          });
          
          $('.content-carousel').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            infinite: true,
            arrows: false,
            draggable: false,
            lazyLoad: 'ondemand',
            initOnLoad: true,
            nextSlidesToPreload: 2,
            fade: true,
            asNavFor: '.modal-carousel-vertical'
          });
          
          $('.preview-window').on('mousewheel', _.debounce(function(e, delta) {
            e.preventDefault();
            if (e.deltaY < 0) {
              $(".modal-carousel-main").slick('slickNext');
            } else {
              $(".modal-carousel-main").slick('slickPrev');
            }
          }, 250));
          
          document.onkeydown = checkKey;
          
          function checkKey(e) {
            e = e || window.event;

            if (e.keyCode == '37') {
              // left arrow
              $(".modal-carousel-outer").slick('slickPrev');
            }
            else if (e.keyCode == '39') {
              // right arrow
              $(".modal-carousel-outer").slick('slickNext');
            }
          }
          
          document.addEventListener('touchstart', handleTouchStart, false);      
          document.addEventListener('touchmove', handleTouchMove, false);

          var xDown = null;                                                        
          var yDown = null;                                                        

          function handleTouchStart(evt) {                                         
              xDown = evt.touches[0].clientX;                                      
              yDown = evt.touches[0].clientY;                                      
          };                                                

          function handleTouchMove(evt) {
              if ( ! xDown || ! yDown ) {
                  return;
              }

              var xUp = evt.touches[0].clientX;                                    
              var yUp = evt.touches[0].clientY;

              var xDiff = xDown - xUp;
              var yDiff = yDown - yUp;

              if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
                  if ( xDiff > 0 ) {
                    // left swipe
                    $(".modal-carousel-outer").slick('slickPrev');
                  } else {
                    // right swipe
                    $(".modal-carousel-outer").slick('slickNext');
                  }                       
              } else {
                  if ( yDiff > 0 ) {
                      /* up swipe */ 
                  } else { 
                      /* down swipe */
                  }                                                                 
              }
              /* reset values */
              xDown = null;
              yDown = null;                                             
          };
          
          $('[data-demo]').click(function () {
            
            $('.targets').addClass('active');
            
            var id = $(this).attr('data-demo');
            var urlNoProtocol = id.replace(/^https?\:\/\//i, "");
            
            var src = id;
            var iframe = '<iframe id="viewer-frame" class="viewer-frame" frameborder="0" src="'+src+'" allowfullscreen></iframe>';
            $(".viewer-target").html(iframe);

            return false;
          });
          
          $('a.back').click(function () {
            setTimeout(function() {
              $(".viewer-target #viewer-frame").remove();
            },
            1000);
            
            $('.targets').removeClass('active');
            return false;
          });
          
        });
      }
    });
    
    $('a.modal-reveal').on('click', function(e) {
      var post_url = $(this).attr("href");
      var post_id = $(this).attr("rel");
      $('#theme-view').html('<div id="loading"></div>');
      $('#theme-view').load(post_url);

      $('.modal').addClass('show');
      bodyControl.addClass('active');
      return false;
      
    });

 });
  
})

//function contentToIframe () {
//    var iframe = document.querySelector('iframe[name="' + this.target + '"]'),
//        curDisplay = iframe.style.display;
//    iframe.src = this.href;
//    if (!curDisplay || curDisplay !== 'block') {
//        iframe.style.display = 'block';
//    }
//}
//var links = document.querySelectorAll('a.demo[target]');
//for (var i = 0, len = links.length; i < len; i++) {
//    links[i].addEventListener('click', contentToIframe);
//}

$(document).ready(function() {
  $('body').on('click','.close-modal',function(){
    $('#theme-view').removeClass('show');
    bodyControl.removeClass("active");
  });
});

$(document).bind('DOMNodeInserted', function(event) {

  $('.preview-window').on('mousewheel', _.debounce(function(e, delta) {
    e.preventDefault();
    if (e.deltaY < 0) {
      $(".modal-carousel").slick('slickNext');
    } else {
      $(".modal-carousel").slick('slickPrev');
    }
  }, 250));
  
});

$(function(){
  //  Get the hash value from the URL
  var hashedValue = window.location.hash;
  history.pushState("", document.title, window.location.pathname);
  if (hashedValue.replace('#', '') == '') {
      hashedValue = 'all';            //  Use the default filter if there's no hash value
  } else {
      hashedValue = '.' + hashedValue.replace('#', '');      //  Try to figure out the filter class based on the hash value
  }
  
  $('#posts').mixItUp({
    selectors: {
      filter: '.post-filter',
      sort: '.post'
    },
    load: {
      filter: hashedValue,
      page: 1
    }
  });
});

jQuery(document).ready(function($) {
  $('body').on('click','.close-modal',function(e){
    $('.comment-response').removeClass('show');
    $('.comment-response').removeClass('thank-you');
    e.preventDefault();
  });
});

jQuery(document).ready(function($) {
	jQuery('a.share-target').on("click", function() {
		newwindow = window.open($(this).attr('href'),'','height=200,width=400');
		if (window.focus) {newwindow.focus()}
		return false;
	});
});

jQuery(document).ready(function($) {
  function shareContainer() {
    var win = $(this); //this = window
    if (win.width() >= 961) {
      if (jQuery(".share-container").length){
        jQuery(".share-container").pin({
          containerSelector: ".container",
          padding: {
            top: 100, 
            bottom: 0
          },
          minWidth: 767
        });
      }
    }
  };

  shareContainer();

  $(window).on('resize', function(){
    shareContainer();
  });
  
  $('#comment-modal, .comment-reply-link, #cancel-comment-reply-link').on("click", function() {
    $('#comment-modal').text(function(i, text){
      
      if ($(this).text() == 'Leave a Reply') {
        $('.comment-respond').addClass('slide');
        return text === "Leave a Reply" ? "Close" : "Leave a Reply";
      }
      if ($(this).text() == 'Close') {
        $('.comment-respond').removeClass('slide');
        return text === "Leave a Reply" ? "Close" : "Leave a Reply";
      }
    })
  });
  
  $('#comment-modal').on("click", function(e) {
    e.preventDefault();
  });  
  
});

jQuery(document).ready(function($) {
  // validate the comment form when it is submitted
  if (jQuery("#commentform").length){
    $("#commentform").validate();
  }
  
  var prev = {
      start: 0,
      stop: 0
  };

  var content = $('.comments > li.comment');

  var Paging = $(".pagination").paging(content.length, {
      onSelect: function() {

        var data = this.slice;

        content.slice(prev[0], prev[1]).removeClass('animate animateIn').addClass('hidden');
        content.slice(data[0], data[1]).removeClass('hidden').addClass('animate animateIn');

        prev = data;

        return true; // locate!
      },
      onFormat: function(type) {

          switch (type) {

              case 'block':

                  if (!this.active)
                      return '<span class="pager page-number disabled"><span>' + this.value + '</span></span>';
                  else if (this.value != this.page)
                      return '<span class="pager page-number"><a href="#' + this.value + '">' + this.value + '</a></span>';
                  return '<span class="pager page-number active"><span>' + this.value + '</span></span>';

              case 'right':
              case 'left':

                  if (!this.active) {
                      return '';
                  }
                  return '<span class="pager page-number"><a href="#' + this.value + '">' + this.value + '</a></span>';

              case 'next':

                  if (this.active) {
                      return '<span class="pager page-next"><a href="#' + this.value + '"><i class="fa fa-arrow-right" aria-hidden="true"></i></a></span>';
                  }
                  return '<span class="pager page-next disabled"><span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></span>';

              case 'prev':

                  if (this.active) {
                      return '<span class="pager page-prev"><a href="#' + this.value + '"><i class="fa fa-arrow-left" aria-hidden="true"></i></a></span>';
                  }
                  return '<span class="pager page-prev disabled"><span><i class="fa fa-arrow-left" aria-hidden="true"></i></span></span>';

              case 'fill':
                  if (this.active) {
                      return "...";
                  }
          }
          return ""; // return nothing for missing branches
      },
      format: '[< ncnnn! >]',
      perpage: 3,
      lapping: 0,
      page: null // we await hashchange() event
  });


  $(window).hashchange(function() {

      if (window.location.hash)
          Paging.setPage(window.location.hash.substr(1));
      else
          Paging.setPage(1); // we dropped "page" support and need to run it by hand
  });

  $(window).hashchange();
  
});

// AJAXified commenting system
jQuery('document').ready(function($){
  var commentform = $('#commentform'); // find the comment form
  var commentbox = $('.comment-box');
  var commentresponse = $('.response-modal');
  commentresponse.prepend('<div id="comment-status" ></div>'); // add info panel before the form to provide feedback or errors
  var statusdiv=$('#comment-status'); // define the infopanel

  commentform.submit(function(){
  //serialize and store form data in a variable
  var formdata=commentform.serialize();
  //Add a status message
  statusdiv.html('<p class="comment-submit">Submitting Comment...</p>');
  //Extract action URL from commentform
  var formurl=commentform.attr('action');
  //Post Form with data
  $.ajax({
    type: 'post',
    url: formurl,
    data: formdata,
    beforeSend: function(){
      $('.comment-response').addClass('show loading');
    },
    error: function(XMLHttpRequest, textStatus, errorThrown){
      statusdiv.html('<p class="wdpajax-error" >You might have left one of the fields blank, or be posting too quickly</p>');
      $('.comment-response').addClass('failed');
      $('.comment-response').removeClass('loading');
    },
    complete: function(){
      $('.comment-response').removeClass('loading');
    },
    success: function(data, textStatus){
      if(data=="success") {
        $('.comment-response').addClass('thank-you');
        statusdiv.html('<div class="ajax-success"><div><i class="fa fa-check-circle" aria-hidden="true"></i></div>Thank you! <span>We&#8217;ve received your comment!</span></div>');
      } else {
        statusdiv.html('<p class="ajax-error" >Please wait a while before posting your next comment</p>');
        commentform.find('textarea[name=comment]').val('');
      }
    }
  });
  return false;

  });
});

jQuery(document).ready(function($) {
  $('a.intro-vid').on("click", function() {
    $('#intro-video').addClass('show');
    $('body').addClass("active");
  });
  $('i.close-modal').on("click", function() {
    $('#intro-video').removeClass('show');
    $('body').removeClass("active");
  });
});

jQuery(document).ready(function($) {
  var hashValue = window.location.hash;
  history.pushState("", document.title, window.location.pathname);
  if (hashValue != "#start") {
    
    var yt_int, yt_players={},
    initYT = function() {
      $(".dragVideo").each(function() {
          yt_players[this.id] = new YT.Player(this.id);
      });
    };
    $.getScript("//www.youtube.com/player_api", function() {
      yt_int = setInterval(function(){
        if(typeof YT === "object"){
            initYT();
            clearInterval(yt_int);
        }
      },500);
    });
    
    $('#resource-video').removeClass('hidden');
    
    $('body').on('click','.play-area',function(e){
      $('#resource-video').addClass('hidden');
      $('#draggable, .show-hide').addClass('shown');
      e.preventDefault();
    }); 
    
    $('body').on('click','.show-hide',function(e){
      $(this).text(function(i, text) {
        if ($('.show-hide').text() == 'Hide Video') {
          yt_players['dragVideo'].pauseVideo();
          $('#draggable').removeClass('shown');
          return text === "Hide Video" ? "Show Video" : "Hide Video";
        }
        if ($('.show-hide').text() == 'Show Video') {
          yt_players['dragVideo'].playVideo();
          $('#draggable').addClass('shown');
          return text === "Hide Video" ? "Show Video" : "Hide Video";
        }
      })
      e.preventDefault();
    }); 
    
    function drag_start(event) {
      var style = window.getComputedStyle(event.target, null);
      event.dataTransfer.setData("text/plain",
      (parseInt(style.getPropertyValue("left"),10) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top"),10) - event.clientY));
    } 
    function drag_over(event) {
      event.preventDefault(); 
      return false; 
    } 
    function drop(event) {
      var offset = event.dataTransfer.getData("text/plain").split(',');
      var dm = document.getElementById('draggable');
      dm.style.left = (event.clientX + parseInt(offset[0],10)) + 'px';
      dm.style.top = (event.clientY + parseInt(offset[1],10)) + 'px';
      event.preventDefault();
      return false;
    }
    var dm = document.getElementById('draggable'); 
    dm.addEventListener('dragstart',drag_start,false); 
    document.body.addEventListener('dragover',drag_over,false); 
    document.body.addEventListener('drop',drop,false); 
  }
});


function initPreviewSlider() {
  "use strict";
  $(".main-slider .slider-inner").owlCarousel({
		navigation : true, // Show next and prev buttons
		slideSpeed : 800,
		paginationSpeed : 400,
		addClassActive: true,
		singleItem:true,
		mouseDrag : false,
		afterInit : jumpEvent,
		beforeMove : addEvent,
		afterAction : removeEvent
  });
}

initPreviewSlider();

$('.small-slider').hover(function(e) {
  $('.main-slider .slide-item').trigger(e.type);
})

function initSmallSlider() {
  "use strict";
  $(".small-slider .slider-inner").owlCarousel({
		navigation : true, // Show next and prev buttons
		slideSpeed : 800,
		paginationSpeed : 400,
		addClassActive: true,
		singleItem:true,
		mouseDrag : false,
		afterInit : jumpEvent,
		beforeMove : addEvent,
		afterAction : removeEvent
 
  });
}
initSmallSlider();

function jumpEvent(elem){
	this.jumpTo(1);
}
function addEvent(elem){
	$(".slide-nav").addClass("moving");
}
function removeEvent(elem){
	setTimeout(function(){
		$(".slide-nav").removeClass("moving");
	}, 800);
}

var owl = $('.slider-wrap .slider-inner');
owl.owlCarousel();

// Custom Navigation Events
$(".next-slide a").click(function(e){
  owl.trigger('owl.next');
  e.preventDefault();
})
$(".prev-slide a").click(function(e){
  owl.trigger('owl.prev');
  e.preventDefault();
})