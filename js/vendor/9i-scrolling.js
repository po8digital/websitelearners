$(function(){
  
  var configOne = {
    origin: 'bottom',
    mobile: true,
    duration: 500,
    opacity: 0,
    delay: 100,
    easing: 'ease-in-out',
    scale: 1,
    distance: '150%'
  }
  
  var configTwo = {
    origin: 'bottom',
    mobile: true,
    duration: 500,
    opacity: 0.5,
    delay: 200,
    easing: 'ease-in-out',
    scale: 1,
    distance: '150%'
  }
  
  var configThree = {
    origin: 'bottom',
    mobile: true,
    duration: 500,
    opacity: 0.5,
    delay: 300,
    easing: 'ease-in-out',
    scale: 1,
    distance: '150%'
  }

  window.sr = ScrollReveal();
  
  sr.reveal('.next-item:nth-child(1)', configOne);
  sr.reveal('.next-item:nth-child(2n)', configTwo);
  sr.reveal('.next-item:nth-child(3n)', configThree);

  
//  sr.reveal('[data-sr]');
});