<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" sizes="16x16">
  
<meta property="og:url" content="http://www.websitelearners.com" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Website Learners" />
<meta property="og:description" content="" />
<meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/" />
<meta name="description" content="" />
<meta name="keywords" content="" />
  
<?php wp_head(); ?>
  
</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1657325081225281";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="page" class="page<?php if(is_blog()) { ?> blog-archive<?php } ?>">
	<header id="header" class="header" role="banner">
    <div class="inner">
      
      <?php if(is_blog()) : ?>
      <div class="logo">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <?php if( get_field('svg_logo_white', 'option') ){ ?>
          <img src="<?php the_field('svg_logo_white', 'option'); ?>" onerror="this.src='<?php the_field('png_logo_white', 'option'); ?>'" alt="Website Learners" width="275" height="42" />
        <?php }else { ?>
          <img src="<?php the_field('png_logo_white', 'option'); ?>" alt="Website Learners" width="275" height="42" />
        <?php } ?>
        </a>
      </div>
      <?php else : ?>
      <div class="logo">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <?php if( get_field('svg_logo', 'option') ){ ?>
          <img src="<?php the_field('svg_logo', 'option'); ?>" onerror="this.src='<?php the_field('png_logo', 'option'); ?>'" alt="Website Learners" width="275" height="42" />
        <?php }else { ?>
          <img src="<?php the_field('png_logo', 'option'); ?>" alt="Website Learners" width="275" height="42" />
        <?php } ?>
        </a>
      </div>
      <?php endif; ?>
      <div class="logo mob">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <?php if( get_field('svg_logo', 'option') ){ ?>
          <img src="<?php the_field('mobile_logo', 'option'); ?>" onerror="this.src='<?php the_field('png_logo', 'option'); ?>'" alt="Website Learners" width="40" height="42" />
        <?php }else { ?>
          <img src="<?php the_field('png_logo', 'option'); ?>" alt="Website Learners" width="40" height="42"/>
        <?php } ?>
        </a>
      </div>
      <?php if(is_blog()) : ?>
      <button class="navigate light"></button>
      <?php else : ?>
      <button class="navigate"></button>
      <?php endif; ?>
      <nav class="main-nav">
        <div class="logo mob">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
          <?php if( get_field('svg_logo', 'option') ){ ?>
            <img src="<?php the_field('mobile_logo', 'option'); ?>" onerror="this.src='<?php the_field('png_logo', 'option'); ?>'" alt="Website Learners" width="40" height="42" />
          <?php }else { ?>
            <img src="<?php the_field('png_logo', 'option'); ?>" alt="Website Learners" width="40" height="42"/>
          <?php } ?>
          </a>
        </div>
        <i class="close icon-close"></i>
        <?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container_class' => 'primary-menu', 'container' => '' ) ); ?>
      </nav>
    </div>
	</header>

	<div class="content">