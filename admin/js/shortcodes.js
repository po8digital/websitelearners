// Tooltip Snippet
(function() {
  tinymce.create('tinymce.plugins.article_snippet', {
    init : function(ed, url) {
      ed.addButton('article_snippet', {
        title : 'Article Snippet',
        icon: 'fa fa-hand-scissors-o',
        onclick : function() {
          ed.selection.setContent('[article_snippet]' + ed.selection.getContent() + '[/article_snippet]');
        }
      });
    },
    createControl : function(n, cm) {
        return null;
    },
  });
  tinymce.PluginManager.add('article_snippet', tinymce.plugins.article_snippet);
})();

// Article Blockquote
(function() {
  tinymce.create('tinymce.plugins.article_quote', {
    init : function(ed, url) {
      ed.addButton('article_quote', {
        title : 'Blockquote',
        icon: 'fa fa-quote-left',
        onclick : function() {
          ed.selection.setContent('[article_quote]' + ed.selection.getContent() + '[/article_quote]');
        }
      });
    },
    createControl : function(n, cm) {
        return null;
    },
  });
  tinymce.PluginManager.add('article_quote', tinymce.plugins.article_quote);
})();

// Info Content Box
(function() {
  tinymce.create('tinymce.plugins.content_box_info', {
    init : function(ed, url) {
      ed.addButton('content_box_info', {
        title : 'Content Box Info',
        icon: 'fa fa-info',
        onclick : function() {
          ed.selection.setContent('[content_box_info]' + ed.selection.getContent() + '[/content_box_info]');
        }
      });
    },
    createControl : function(n, cm) {
        return null;
    },
  });
  tinymce.PluginManager.add('content_box_info', tinymce.plugins.content_box_info);
})();

// Alert Content Box
(function() {
  tinymce.create('tinymce.plugins.content_box_alert', {
    init : function(ed, url) {
      ed.addButton('content_box_alert', {
        title : 'Content Box Alert',
        icon: 'fa fa-exclamation',
        onclick : function() {
          ed.selection.setContent('[content_box_alert]' + ed.selection.getContent() + '[/content_box_alert]');
        }
      });
    },
    createControl : function(n, cm) {
        return null;
    },
  });
  tinymce.PluginManager.add('content_box_alert', tinymce.plugins.content_box_alert);
})();

// Checkmark Content Box
(function() {
  tinymce.create('tinymce.plugins.content_box_checkmark', {
    init : function(ed, url) {
      ed.addButton('content_box_checkmark', {
        title : 'Content Box Checkmark',
        icon: 'fa fa-check',
        onclick : function() {
          ed.selection.setContent('[content_box_checkmark]' + ed.selection.getContent() + '[/content_box_checkmark]');
        }
      });
    },
    createControl : function(n, cm) {
        return null;
    },
  });
  tinymce.PluginManager.add('content_box_checkmark', tinymce.plugins.content_box_checkmark);
})();

// Lightbulb Content Box
(function() {
  tinymce.create('tinymce.plugins.content_box_lightbulb', {
    init : function(ed, url) {
      ed.addButton('content_box_lightbulb', {
        title : 'Content Box Lightbulb',
        icon: 'fa fa-lightbulb-o',
        onclick : function() {
          ed.selection.setContent('[content_box_lightbulb]' + ed.selection.getContent() + '[/content_box_lightbulb]');
        }
      });
    },
    createControl : function(n, cm) {
        return null;
    },
  });
  tinymce.PluginManager.add('content_box_lightbulb', tinymce.plugins.content_box_lightbulb);
})();

(function($) {
  $(document).ready(function() {
    $('body').on('mousedown', '#wp_delimgbtn', function(e) {
      var editor = tinyMCE.activeEditor, 
          element = editor.selection.getNode();

      if(element.tagName !== 'FIGURE') {
        $(element).parents('figure').remove();
      }
    });
  });
}(jQuery));