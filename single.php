<?php get_header(); 

  global $post, $wp_query, $comment;
  $postid = get_the_ID();

?>

<div class="main" role="main">

  <!-- Page Header -->
  <div class="page themes-page">
    <article class="post single">
    <?php

      $id                 = get_queried_object_id();
      $featured           = get_post_thumbnail_id();
      $featured_url_array = wp_get_attachment_image_src($featured, 'thumbnail-size', true);
      $featured_url       = $featured_url_array[0];

    ?>
    <div class= "article-banner" style="background-image: url('<?php echo $featured_url; ?>');" itemscope="" itemtype="http://schema.org/BlogPosting">
      <div class="post">
        <div class="container">
          <div class="entry-title">
            <h1 class="post-title" itemprop="name headline"><?php the_title(); ?></h1>
          </div>
          <div class="post-meta">
          <?php 

              $author_id      = $post->post_author;
              $user_email     = get_the_author_meta( 'user_email', $author_id );

              $hash       = md5( strtolower( trim ( $user_email ) ) );
              $avatar_url = 'https://gravatar.com/avatar/' . $hash;
            
              $date       = get_the_date('F j, Y');
            
              $display_name = get_the_author_meta( 'display_name', $post->post_author );

            ?>
            <div class="post-stamp">
              <div class="inner">
                <span class="pre-stamp faded-text">Posted on</span>
                <span class="date" itemprop="datePublished"><?php echo $date; ?></span>
                <span class="faded-text">by</span>
              </div>
            </div>
            <div class="photo">
              <img src="<?php echo $avatar_url; ?>" alt="<?php echo $display_name; ?>" />
            </div>
            <div class="author vcard" itemprop="author" itemscope="" itemtype="http://schema.org/Person">
              <span itemprop="name"><?php echo $display_name; ?></span>
            </div>
            <div class="post-cat">
              <div class="inner">
                <span class="faded-text">in</span>
                <?php
                  $categories = get_the_category();
                  $separator = ' ';
                  $output = '';
                  if ( ! empty( $categories ) ) {
                      foreach( $categories as $category ) {
                          $output .= '<a rel="category tag" class="category" itemprop="articleSection" href="/blog/#' . esc_html( $category->slug ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
                      }
                      echo trim( $output, $separator );
                  }
                ?>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="post-content">
        <div class="container">
          <div class="share-container">
            <span class="share-title">Share</span>
            <div class="share-article">
              <?php
                // Get current page URL 
                $crunchifyURL = urlencode(get_permalink($post->ID));
                // Get current page title
                $crunchifyTitle = str_replace( ' ', '%20', get_the_title());
                // Construct sharing URL without using any script
                $twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL.'&amp;via=websitelearners';
                $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL;
                
              ?>
              <ul>
                <li><a class="mail" href="mailto:type email address here?subject=I wanted to share this post with you from <?php bloginfo('name'); ?>&body=<?php the_title('','',true); ?>&#32;&#32;<?php echo $crunchifyURL; ?>" title="Email to a friend/colleague"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                <li><a class="share-target facebook" href="<?php echo $facebookURL ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a class="share-target twitter" href="<?php echo $twitterURL ?>" ><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="inner">
            
          <?php while ( have_posts() ) : the_post(); ?>
            <div class="post-main">
            <?php the_content(); ?>

            <?php endwhile; // End of the loop. ?>
            </div>
            <div class="author-wrap">
              <div class="author-inner">
                <div class="author-photo">
                  <?php 

                    $full_name = get_author_name( $id );
                  
                    // only output if gravatar function exists
                    if (function_exists('get_avatar')) : ?>

                    <?php
                    // Convert email into md5 hash and set image size to 32px
                    $gravatar = 'https://www.gravatar.com/avatar/' . md5(strtolower($user_email)) . '&s=32';

                    // Convert email into md5 hash and remove image size to use as post image
                    $gravatar_bg = 'https://www.gravatar.com/avatar/' . md5(strtolower($user_email)) . '';
                    ?>
                    <!-- Output the gravatar as img src -->
                    <img src="<?php echo "$gravatar";?>" alt="">

                    <!-- Output the gravatar as background url -->
                    <div class="avatar" style="background: url(<?php echo $gravatar_bg ?>);" ></div>
                  <?php else : ?>
                    <img src="<?php echo $avatar_url; ?>" alt="<?php echo $autor_name; ?>" />
                  <?php endif; ?>
                </div>
                <div class="author-info">
                  <div class="author-name"><?php echo $display_name; ?></div>
                  <div class="author-bio"><?php the_author_description(); ?></div>
                </div>
              </div>
            </div>
            <?php 
              $subscribe_title   = get_field('subscribe_block_title');
              $subscribe_list_id = get_field('subscribe_block_list_id');
            
              if($subscribe_title) : 
            ?>
            <div class="subscribe-wrap">
              <div class="subscribe-title"><?php echo $subscribe_title ?></div>
              <form method="post" class="af-form-wrapper subscribe" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" target="_blank" >
                <div style="display: none;">
                <input type="hidden" name="meta_web_form_id" value="635567117" />
                <input type="hidden" name="meta_split_id" value="" />
                <input type="hidden" name="listname" value="awlist<?php echo $subscribe_list_id ?>" />
                <input type="hidden" name="redirect" value="https://websitelearners.com/please-confirm-your-email/" id="redirect_ab439d9b075db7596cd59387c49bfc0a" />
                <input type="hidden" name="meta_adtracking" value="resource" />
                <input type="hidden" name="meta_message" value="1" />
                <input type="hidden" name="meta_required" value="email" />
                <input type="hidden" name="meta_tooltip" value="" />
                </div>

                <input class="text" id="awf_field-86169729" type="text" name="email" placeholder="Your Email Address" value="" tabindex="500" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                <input name="submit" class="submit btn green medium" type="submit" value="Subscribe" tabindex="501" />
                <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=bMysrGzsjIzs" alt="" /></div>
              </form>
            </div>
            <?php endif; ?>
            
            <div class="comments-wrap">
              <div class="comments-header">
                <h5 id="comments" class="h5"><?php comments_number('<span>No</span> Responses', '<span>1</span> Comment', '<span>%</span> Comments' );?></h5>
                
                <a class="btn big grey" id="comment-modal" href="#">Leave a Reply</a>
              </div>
                                      
              <div class="reply-box">
                <div class="comment-box">
                 <?php 
                  $fields = array(
                    'author' =>
                        '<p class="comment-form-author">' .
                        '<label for="author">' . __( 'Your Name', 'domainreference' ) . '</label> ' .
                        ( $req ? '<span class="required">*</span>' : '' ) .
                        '<input id="author" class="required" name="author" placeholder="Type in here.." type="text" value="' . esc_attr( $commenter['comment_author'] ) .
                        '" size="30"' . $aria_req . ' /></p>',

                      'email' =>
                        '<p class="comment-form-email"><label for="email">' . __( 'Your Email Address', 'domainreference' ) . '</label> ' .
                        ( $req ? '<span class="required">*</span>' : '' ) .
                        '<input id="email" class="required email" name="email" type="text" placeholder="Type in here.." value="' . esc_attr(  $commenter['comment_author_email'] ) .
                        '" size="30"' . $aria_req . ' /></p>'
                  ); 
                  $form_args = array(
                    'fields' => apply_filters( 'comment_form_default_fields', $fields),
                    'label_submit'=>__('Submit Comment'),
                    'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Your Comment', 'noun' ) . '</label><textarea class="required" id="comment" placeholder="Type in here.." name="comment" cols="45" rows="4" aria-required="true"></textarea></p>'
                  );
                  ob_start();
                  $comment_form = comment_form($form_args, $postid);
                  $comment_form = ob_get_clean();
                  //insert code to modify $comment_form
                  echo $comment_form;
                ?>
                  
                </div>
              </div> 
              <?php
              $page = intval( get_query_var( 'cpage' ) );
              if ( 0 == $page ) {
                $page = 1;
                set_query_var( 'cpage', $page );
              }
              $comments_per_page = -1;
              $comments          = get_comments( array( 
                'status' => 'approve',
                'post_id' => $postid,
              ) );
              ?>
              <ol class="comments" start="<?php echo $comments_per_page * $page - $comments_per_page + 1 ?>">
                
              <?php
                $com_args = array(
                  'max_depth'         => '2',
                  'style'             => 'ol',
                  'callback'          => 'custom_comments',
                  'type'              => 'all',
                  'reply_text'        => 'Reply',
                  'per_page'          => $comments_per_page,
                  'page'              => $page,
                  'reverse_top_level' => false,
                  'avatar_size'       => 96,
                  'format'            => 'html5',
                  'short_ping'        => false,
                  'echo'              => true
                ); 

                wp_list_comments($com_args, $comments);
              ?>
              </ol>
              <div class="paginate">
                <div class="pagination"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>
  </div>
  
</div>
<?php
get_footer(); ?>