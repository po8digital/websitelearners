<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Website_Learners
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<?php
// You can start editing here -- including this comment!
if ( have_comments() ) : ?>
  <?php
    $args = array(
        'status' => 'approve',
        'number' => '15', // whatever number of comments you wish to display.
        'post_id' => 18, // use post_id, not post_ID - replace 18 by your post ID
    );

    $comments = get_comments($args);

    // Browse all comments one by one and display the content :
    foreach ($comments as $comment) :
        echo($comment->comment_content);
    endforeach;

endif;

comment_form(); ?>

