  </div><!-- .content -->

  <?php  if ( is_post_type_archive('themes') || is_tax( 'categories' ) ) : ?>
  <!-- Thmemes Modal -->
  <div class="modal theme-modal targets" id="theme-view"></div>
  <div class="theme-viewer targets">
    <a href="#" class="back">Back <i class="caret"></i></a>
    <div class="viewer-target"></div>
  </div>
  <?php endif; ?>
  <?php if(is_blog()) : ?>
  <div class="modal comment-response">
    <div class="response-modal">
      <i class="close-modal"></i>
    </div>
  </div>
  <?php endif; ?>
  <?php if(is_page_template( 'page-home.php' )) : ?>
    <?php $video_id = get_field('home_button_link'); if($video_id) : ?>
    <div class="modal video-modal" id="intro-video">
      <div class="video-holder">
        <i class="close-modal"></i>
        <iframe width="560" class="youtube-player" type="text/html" height="315" src="https://www.youtube.com/embed/<?php echo $video_id; ?>"></iframe>
      </div>
    </div>
    <?php endif; ?>
  <?php endif; ?>

  <?php if(is_page_template( 'page-resource.php' )) : ?>
    <?php $modal_video = get_field('resource_modal_video'); if($modal_video) : ?>
    <div class="modal action-modal hidden" id="resource-video">
      <div class="modal-holder">
        <div class="video-action">
          <div class="play-area">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/play.svg" alt="How to Video" width="180" height="110" />
          </div>
          <p>Click here to watch the <strong>video</strong></p>
        </div>
      </div>
    </div>
    <div class="video-holder" id="draggable" draggable="true">
      <i class="fa fa-arrows dragit" aria-hidden="true"></i>
      <iframe width="560" class="dragVideo" id="dragVideo" height="315" src="//www.youtube.com/embed/<?php echo $modal_video; ?>?enablejsapi=1" allowfullscreen></iframe>
    </div>
    <button class="show-hide">Hide Video</button>
    <?php endif; ?>
  <?php endif; ?>
	<footer class="footer" role="contentinfo">
    <div class="inner">
      <div class="site-info">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <?php if( get_field('svg_logo', 'option') ){ ?>
          <img src="<?php the_field('svg_logo_footer', 'option'); ?>" onerror="this.src='<?php the_field('png_logo_footer', 'option'); ?>'" alt="Website Learners" width="200" height="32" />
        <?php }else { ?>
          <img src="<?php the_field('png_logo_footer', 'option'); ?>" alt="Website Learners" width="200" height="32" />
        <?php } ?>
        </a>
      </div>
      <div class="columns">
        <div class="col">
          <h3 class="col-heading">Resources</h3>
          <?php wp_nav_menu( array( 'theme_location' => 'footer-menu-one', 'container_class' => 'footer-menu-one', 'container' => '' ) ); ?>
        </div>
        <div class="col">
          <h3 class="col-heading">Website Build Guides</h3>
          <?php wp_nav_menu( array( 'theme_location' => 'footer-menu-two', 'container_class' => 'footer-menu-two', 'container' => '' ) ); ?>
        </div>
        <div class="col">
          <h3 class="col-heading">From the Blog</h3>
          <?php $loop = new WP_Query('showposts=1&orderby=ID&order=DESC'); ?>
          <?php if($loop->have_posts()): while($loop->have_posts()): $loop->the_post(); ?>
          
          <?php 
            $string   = get_the_content();
            $content  = (strlen($string) > 60) ? substr($string,0,57).'..' : $string;
          ?>
          <article class="post" id="post-<?php the_ID(); ?>">
            <h1 class="title"><?php the_title(); ?></h1>
            <p class="snippet"><?php echo $content; ?></p>
            <a href="<?php the_permalink(); ?>" class="read-more">Read full article</a>
          </article>
          <?php endwhile; else: ?>
            <p>No recent posts yet!</p>
          <?php endif; ?>
        </div>
        <div class="col">
          <h3 class="col-heading">Connect with us</h3>
          <?php $number = get_field('contact_number', 'option'); ?>
          <div class="connect" itemscope>
            <span itemprop="tel" class="number"><?php echo $number; ?></span>
            <a href="<?php echo esc_url( home_url( '' ) ); ?>/contact" itemprop="url" class="contact-link">Contact Us</a>
            
            <?php if( have_rows('social_connect', 'option') ): ?>
            <ul class="networks">
              <?php while ( have_rows('social_connect', 'option') ) : the_row(); ?>
              <?php 
                $name = get_sub_field('name');
                $icon = get_sub_field('icon');
                $link = get_sub_field('link');  
              ?>

              <li><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><i class="fa <?php echo $icon; ?>"></i></a></li>
              <?php endwhile; ?>
            </ul>
            <?php else : ?>
            
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
