<?php get_header(); 

  global $post;

?>

<div class="main" role="main">
  <div class="page">
    <?php 
      $featured = array(
        'post_type' => 'post',
        'posts_per_page' => 1,
        'meta_query' => array( 
          array(
            'key' => 'featured_post',
            'value' => '1'
          )
        )
      );
      $featured_query = new WP_Query($featured);
    ?>
    <?php while ( $featured_query->have_posts() ) : $featured_query->the_post();  ?>
    <?php
    
      $id                 = get_queried_object_id();
      $featured           = get_post_thumbnail_id();
      $featured_url_array = wp_get_attachment_image_src($featured, 'thumbnail-size', true);
      $featured_url       = $featured_url_array[0];
    
      $terms = get_the_terms( $post->ID, 'category' );
      $term = array_pop($terms);
    
    ?>
    <article class="featured-article" style="background-image: url('<?php echo $featured_url; ?>');" itemscope="" itemtype="http://schema.org/BlogPosting">
      <div class="post">
        <div class="container">
          <div class="featured-tag">
            <span class="featured"><?php echo $term->name; ?></span>
          </div>
          <div class="entry-title">
            <h1 class="post-title" itemprop="name headline">
              <a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
            </h1>
          </div>
          <div class="post-meta">
          <?php 
              $date       = get_the_date('F j, Y');

              $fname = get_the_author_meta('first_name');
              $lname = get_the_author_meta('last_name');
              $full_name = '';
              if( empty($fname)){
                  $full_name = $lname;
              } elseif( empty( $lname )){
                  $full_name = $fname;
              } else {
                  //both first name and last name are present
                  $full_name = "{$fname} {$lname}";
              }
            ?>
            <div class="post-stamp">
              <div class="inner">
                <span class="date" itemprop="datePublished"><?php echo $date; ?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>
    <?php endwhile; // End Loop ?>
    <?php wp_reset_postdata(); ?>
    <div class="post-filters">
      <button class="filter-it" id="toggle">Filter Posts <i class="caret"></i></button>
      <div class="inner filters">
        <button class="post-filter" data-filter="all">Recent</button>
        <?php 
          $args = array(
            'orderby' => 'id',
            'hide_empty'=> 0
          );
          $categories = get_categories(array(
            'hide_empty' => true
          ));
          foreach ($categories as $cat) :

          $cat_slug = $cat->slug;
          $cat_id   = $cat->term_id;
          $cat_name = $cat->name;
          $cat_desc = $cat->description;

          if ( $cat_id == 1 )
            continue; // skip 'uncategorized'

          ?>

        <button type="button" data-filter=".<?php echo $cat_slug;?>" class="post-filter"><?php echo $cat_name; ?></button>

          <?php endforeach; ?>
      </div>
    </div>
    <div class="container">
      <div class="posts flex wrap-items" id="posts">
        <?php 

        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

        $custom_args = array(
          'post_type' => 'post',
          'posts_per_page' => -1,
          'paged' => $paged
        );

        $custom_query = new WP_Query( $custom_args ); 
        
        ?>

        <?php if ( $custom_query->have_posts() ) : ?>
        
        <!-- the loop -->
        <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
        
        <?php 
          $id    = get_the_ID();
          $categories = get_the_category();
        
          $separator  = ' ';
          $output     = '';

        ?>
        <article class="mix post<?php foreach ($categories as $category) { echo ' ' . $category->slug; } ?>" itemscope="" itemtype="http://schema.org/BlogPosting" data-myorder="<?php echo $id; ?>">
          <div class="inner">
            <?php 
              $featured           = get_post_thumbnail_id();
              $featured_url_array = wp_get_attachment_image_src($featured, $size = 'large', true);
              $featured_url       = $featured_url_array[0];

              $date       = get_the_date('F j, Y');
            ?>
            <?php if($featured_url) :?>
            <div class="image-holder grid-middle column half" style="background-image: url('<?php echo $featured_url; ?>');">
              <a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <img itemprop="image" src="<?php echo $featured_url ?>" alt="<?php the_title(); ?>" />
              </a>
            </div>
            <?php endif; ?>
            <div class="post-body grid-middle column <?php if($featured_url){ ?>half<?php } ?>">
              <div class="category">
                <?php
                  if ( ! empty( $categories ) ) {
                      foreach( $categories as $category ) {
                          $output .= '<span rel="category tag" class="category" itemprop="articleSection" href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</span>' . $separator;
                      }
                      echo trim( $output, $separator );
                  }
                ?>
              </div>
              <div class="post-date">
                <span class="date" itemprop="datePublished"><?php echo $date; ?></span>
              </div>
              <h1 class="entry-title" itemprop="name headline">
                <?php

                  $title = get_the_title();
                ?>
                <a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo mb_strimwidth($title, 0, 44, '...'); ?></a>
              </h1>
              <div class="post-meta">
                <?php 
                  $author_id      = $post->post_author;
                  $user_email     = get_the_author_meta( 'user_email', $author_id );

                  $hash       = md5( strtolower( trim ( $user_email ) ) );
                  $avatar_url = 'https://gravatar.com/avatar/' . $hash;

                  $fname = get_the_author_meta('first_name');
                  $lname = get_the_author_meta('last_name');
                  $full_name = '';
                  if( empty($fname)){
                      $full_name = $lname;
                  } elseif( empty( $lname )){
                      $full_name = $fname;
                  } else {
                      //both first name and last name are present
                      $full_name = "{$fname} {$lname}";
                  }
                ?>
                <div class="photo">
                  <img src="<?php echo $avatar_url; ?>" alt="<?php echo $display_name; ?>" />
                </div>
                <div class="post-info">
                  <div class="author vcard" itemprop="author" itemscope="" itemtype="http://schema.org/Person">
                    <span itemprop="name"><?php echo $full_name; ?></span>
                  </div>
                 </div>
              </div>
            </div>
          </div>
        </article>
        <?php endwhile; ?>

        <?php wp_reset_postdata(); ?>

        <?php else:  ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>

<?php get_footer();
