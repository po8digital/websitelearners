<?php

/* ===============================================================
  Definition Shortcode
=============================================================== */
// SHORTCODES

//Article Snippet
function article_snippet( $atts, $content = null ) {
  
  extract(shortcode_atts(array(
    'text' => 'Text'
  ), $atts));
  
  
  return '<div class="article_snippet">'.$content.'</div>';
  
}
add_shortcode("article_snippet", "article_snippet");
	
add_action('init', 'add_article_snippet_button');
function add_article_snippet_button() {
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
   {
     add_filter('mce_external_plugins', 'add_article_snippet_plugin');
     add_filter('mce_buttons', 'register_article_snippet_button');
   }
}

function register_article_snippet_button($buttons) {
   array_push($buttons, "article_snippet");
   return $buttons;
}

function add_article_snippet_plugin($plugin_array) {
   $plugin_array['article_snippet'] = get_bloginfo('template_url').'/js/shortcodes.js';
   return $plugin_array;
}

//Article Blockquote
function article_quote( $atts, $content = null ) {
  
  extract(shortcode_atts(array(
    'text' => 'Text'
  ), $atts));
  
  
  return '<blockquote class="article_quote">'.$content.'</blockquote>';
  
}
add_shortcode("article_quote", "article_quote");
	
add_action('init', 'add_article_quote_button');
function add_article_quote_button() {
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
   {
     add_filter('mce_external_plugins', 'add_article_quote_plugin');
     add_filter('mce_buttons', 'register_article_quote_button');
   }
}

function register_article_quote_button($buttons) {
   array_push($buttons, "article_quote");
   return $buttons;
}

function add_article_quote_plugin($plugin_array) {
   $plugin_array['article_quote'] = get_bloginfo('template_url').'/js/shortcodes.js';
   return $plugin_array;
}

// Info Content Box
function content_box_info( $atts, $content = null ) {
  
  extract(shortcode_atts(array(
    'text' => 'Text'
  ), $atts));
  
  
  return '<div class="content-box info"><div class="box-img"><img src="'. get_bloginfo("template_url") .'/js/images/info.svg" alt=="Info Content Box" /></div><div class="box-inner">'.$content.'</div></div>';
  
}
add_shortcode("content_box_info", "content_box_info");
	
add_action('init', 'add_content_box_info_button');
function add_content_box_info_button() {
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
   {
     add_filter('mce_external_plugins', 'add_content_box_info_plugin');
     add_filter('mce_buttons', 'register_content_box_info_button');
   }
}

function register_content_box_info_button($buttons) {
   array_push($buttons, "content_box_info");
   return $buttons;
}

function add_content_box_info_plugin($plugin_array) {
   $plugin_array['content_box_info'] = get_bloginfo('template_url').'/js/shortcodes.js';
   return $plugin_array;
}

// Alert Content Box
function content_box_alert( $atts, $content = null ) {
  
  extract(shortcode_atts(array(
    'text' => 'Text'
  ), $atts));
  
  
  return '<div class="content-box alert"><div class="box-img"><img src="'. get_bloginfo("template_url") .'/js/images/exclamation.svg" alt=="Info Content Box" /></div><div class="box-inner">'.$content.'</div></div>';
  
}
add_shortcode("content_box_alert", "content_box_alert");
	
add_action('init', 'add_content_box_alert_button');
function add_content_box_alert_button() {
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
   {
     add_filter('mce_external_plugins', 'add_content_box_alert_plugin');
     add_filter('mce_buttons', 'register_content_box_alert_button');
   }
}

function register_content_box_alert_button($buttons) {
   array_push($buttons, "content_box_alert");
   return $buttons;
}

function add_content_box_alert_plugin($plugin_array) {
   $plugin_array['content_box_alert'] = get_bloginfo('template_url').'/js/shortcodes.js';
   return $plugin_array;
}

// Checkmark Content Box
function content_box_checkmark( $atts, $content = null ) {
  
  extract(shortcode_atts(array(
    'text' => 'Text'
  ), $atts));
  
  
  return '<div class="content-box checkmark"><div class="box-img"><img src="'. get_bloginfo("template_url") .'/js/images/checkmark.svg" alt=="Info Content Box" /></div><div class="box-inner">'.$content.'</div></div>';
  
}
add_shortcode("content_box_checkmark", "content_box_checkmark");
	
add_action('init', 'add_content_box_checkmark_button');
function add_content_box_checkmark_button() {
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
   {
     add_filter('mce_external_plugins', 'add_content_box_checkmark_plugin');
     add_filter('mce_buttons', 'register_content_box_checkmark_button');
   }
}

function register_content_box_checkmark_button($buttons) {
   array_push($buttons, "content_box_checkmark");
   return $buttons;
}

function add_content_box_checkmark_plugin($plugin_array) {
   $plugin_array['content_box_checkmark'] = get_bloginfo('template_url').'/js/shortcodes.js';
   return $plugin_array;
}

// Lightbulb Content Box
function content_box_lightbulb( $atts, $content = null ) {
  
  extract(shortcode_atts(array(
    'text' => 'Text'
  ), $atts));
  
  
  return '<div class="content-box lightbulb"><div class="box-img"><img src="'. get_bloginfo("template_url") .'/js/images/lightbulb.svg" alt=="Info Content Box" /></div><div class="box-inner">'.$content.'</div></div>';
  
}
add_shortcode("content_box_lightbulb", "content_box_lightbulb");
	
add_action('init', 'add_content_box_lightbulb_button');
function add_content_box_lightbulb_button() {
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
   {
     add_filter('mce_external_plugins', 'add_content_box_lightbulb_plugin');
     add_filter('mce_buttons', 'register_content_box_lightbulb_button');
   }
}

function register_content_box_lightbulb_button($buttons) {
   array_push($buttons, "content_box_lightbulb");
   return $buttons;
}

function add_content_box_lightbulb_plugin($plugin_array) {
   $plugin_array['content_box_lightbulb'] = get_bloginfo('template_url').'/js/shortcodes.js';
   return $plugin_array;
}