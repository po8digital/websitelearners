<?php


/* ===============================================================
==================================================================

I.  admin.php

    1.  po8_alter_manage_posts_columns($columns)
        - add new column to posts

    2.  po8_manage_posts_custom_columns($column, $post_id)
        - fill in content of custom columns

    3.  po8_change_post_menu_label()
        - change "posts" label to "articles"

==================================================================
=============================================================== */



/* ===============================================================
    add new column to posts
=============================================================== */

function po8_alter_manage_posts_columns($columns)
{
    $columns['my-new-column'] = 'My New Column';
    return $columns;
}
//add_filter('manage_posts_columns', 'po8_alter_manage_posts_columns', 10);

/* ===============================================================
    fill in content of custom columns
=============================================================== */
function po8_manage_posts_custom_columns($column, $post_id)
{
    switch($column) {
        case 'my-new-column':
            echo "Stuff in my new column";
            break;
    }
}
//add_action('manage_posts_custom_column' , 'po8_manage_posts_custom_columns', 10, 2);

/* ===============================================================
    change "posts" label to "articles"
=============================================================== */
function po8_change_post_menu_label()
{
    global $menu;
    global $submenu;
    $menu[5][0] = 'Articles';
    $submenu['edit.php'][5][0] = 'Articles';
    $submenu['edit.php'][10][0] = 'Add Article';
    $submenu['edit.php'][16][0] = 'Article Tags';
}
function po8_change_post_object_label()
{
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Articles';
    $labels->singular_name = 'Article';
    $labels->add_new = 'Add Article';
    $labels->add_new_item = 'Add New Article';
    $labels->edit_item = 'Edit Article';
    $labels->new_item = 'New Article';
    $labels->view_item = 'View Article';
    $labels->search_items = 'Search Articles';
    $labels->not_found = 'No articles found';
    $labels->not_found_in_trash = 'No articles found in Trash';
}
//add_action('init', 'po8_change_post_object_label');
//add_action('admin_menu', 'po8_change_post_menu_label');


/* ===============================================================
    Hide ACF for all users with the exception of "developer"
=============================================================== */

function remove_menus() {
	global $menu;
	global $current_user;
	get_currentuserinfo();

	if($current_user->user_nicename != 'developer'){

		remove_menu_page( 'pods' );	//PODS															
		remove_menu_page( 'edit.php?post_type=acf-field-group' );	//ACF

	}
}
add_action('admin_menu', 'remove_menus', 999);
