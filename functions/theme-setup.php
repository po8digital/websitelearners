<?php
/* == custom styles for wysiwyg editor =============================================== */
//add_editor_style('editor-style.css');

/* == make sure rss info is added to head ============================================ */
add_theme_support('automatic-feed-links');


/* == add WP 3.0 menu support ======================================================== */
add_theme_support('menus');


/* == add thumbnail and featured image support ======================================= */
add_theme_support('post-thumbnails');


/* == remove admin bar =============================================================== */
add_filter('show_admin_bar', '__return_false');


/* == Removes the default link on attachments  ======================================= */
update_option('image_default_link_type', 'none');


/* == Remove the version number of WP  =============================================== */
remove_action('wp_head', 'wp_generator');


/* == remove funky formatting caused by tinymce advanced ============================= */
function fixtinymceadv($text)
{
    $text = str_replace('<p><br class="spacer_" /></p>','<br />',$text);
    return $text;
}
add_filter('the_content',  'fixtinymceadv');

/* == Obscure login screen error messages ============================================ */
function login_obscure()
{
    return '<strong>Sorry</strong>: Information that you have entered is incorrect.';
}
add_filter('login_errors', 'login_obscure');

/* == add wp-admin custom styling ======================================= */
add_action( 'admin_enqueue_scripts', 'load_admin_style' );
function load_admin_style() {
	wp_register_style( 'admin_css', get_template_directory_uri() . '/editor-style.css', false, '1.0.0' );
	wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/editor-style.css', false, '1.0.0' );
}


/* == Add blog name to title ========================================================= */
function po8_alter_title($title, $sep) {
    $title .= get_bloginfo('name');
    return $title;
}
add_filter('wp_title', 'po8_alter_title', 10, 2);

/* == Queue up all css & js files ==================================================== */
function po8_scripts_styles() {
	if ( !is_admin() ) { 
    wp_deregister_script('jquery'); 
    wp_register_script('jquery', ("//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"), false);
    wp_enqueue_script('jquery'); 
  }
	
	wp_enqueue_script('po8_vendors', get_template_directory_uri() . '/js/vendors.min.js',array('jquery'), null, true);
	
	if ( is_page_template( 'page-form.php' ) ) {
    wp_enqueue_script( 'typeform', '//s3-eu-west-1.amazonaws.com/share.typeform.com/embed.js', array(), '1.0', true); 
  }
	
	if ( is_single() ) {
    wp_enqueue_script('ajaxValidate', '//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js', array('jquery'), '1.16.0'); 
  }
	
	wp_enqueue_script('po8_theme', get_template_directory_uri() . '/js/theme.min.js',array('jquery'), null, true);
	wp_enqueue_style('po8_style', get_template_directory_uri() . '/css/global.min.css', false, null);
	if (is_singular('post')){
			wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'po8_scripts_styles');

/* == adds iOS icons and favicon ===================================================== */
function po8_header_icons() {
    $output = '';
    $output .= '<link rel="apple-touch-icon" sizes="144x144" href="' . $fav144 . '" />' . "\n";
    $output .= '<link rel="apple-touch-icon" sizes="114x114" href="' . $fav114 . '" />' . "\n";
    $output .= '<link rel="apple-touch-icon" sizes="72x72" href="' . $fav72 . '" />' . "\n";
    $output .= '<link rel="apple-touch-icon" href="' . $fav57 . '" />' . "\n";
    $output .= '<link rel="shortcut icon" href="' . $favdef . '" />' . "\n";
    echo $output;
}
add_action('wp_head', 'po8_header_icons');

function is_blog () {
	global $post;
	$posttype = get_post_type($post);
	return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
}

//hide wordpress admin menu
add_filter('show_admin_bar', '__return_false');

//Dynamic Copyright
function dynamic_copyright() {
	global $wpdb;
	$copyright_dates = $wpdb->get_results("
		SELECT
		YEAR(min(post_date_gmt)) AS firstdate,
		YEAR(max(post_date_gmt)) AS lastdate
		FROM
		$wpdb->posts
		WHERE
		post_status = 'publish'
	");
	$output = '';
	if($copyright_dates) {
		$copyright = "Copyright &copy; " . $copyright_dates[0]->lastdate;
		$output = $copyright;
	}
	return $output;
}

//featued image support
add_theme_support('post-thumbnails'); 

// Ajax Comments

add_action('comment_post', 'ajaxify_comments',20, 2);
function ajaxify_comments($comment_ID, $comment_status){
  if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    //If AJAX Request Then
    switch($comment_status){
    case '0':
    //notify moderator of unapproved comment
    wp_notify_moderator($comment_ID);
    case '1': //Approved comment
    echo "success";
    $commentdata=&get_comment($comment_ID, ARRAY_A);
    $post=&get_post($commentdata['comment_post_ID']);
    wp_notify_postauthor($comment_ID, $commentdata['comment_type']);
    break;
    default:
    echo "error";
    }
    exit;
  }
}

// Comment Box Functions
function wpb_move_comment_field_to_bottom( $fields ) {
  $comment_field = $fields['comment'];
  unset( $fields['comment'] );
  $fields['comment'] = $comment_field;
  return $fields;
}
add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

remove_filter('pre_user_description', 'wp_filter_kses');

function custom_comments( $comment, $args, $depth ) {
  $GLOBALS['comment'] = $comment;
  switch( $comment->comment_type ) :
      case 'pingback' :
      case 'trackback' : ?>
          <li <?php comment_class(); ?> id="comment<?php comment_ID(); ?>">
          <div class="back-link"><?php comment_author_link(); ?></div>
      <?php break;
      default : 
            
          ?>
          <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
          
          <div id="loader"></div>
            
          <article <?php comment_class(); ?> class="comment">

          <div class="author-pp">
            <div class="author-photo">
            <?php echo get_avatar( $comment, 96 ); ?>
            </div>
            <span class="author-tag">Author</span>
          </div>
            
          <div class="comment-body">
            <div class="comment-head">
              <span class="author-name"><?php comment_author(); ?></span>
              <?php $comment_date = human_time_diff(get_comment_time('U'), current_time('timestamp'));  ?>
              <span class="comment-date"><?php echo $comment_date; ?></span>
            </div>
            <div class="comment-content"><?php comment_text(); ?></div>
            <div class="reply">
            <?php 
              comment_reply_link( array_merge( $args, array( 
                'reply_text' => 'Reply',
                'depth' => $depth,
                'max_depth' => $args['max_depth'] ,
                'before'  => '',
                'after'  => '',
              ) ) ); ?>
            </div><!-- .reply -->
          </div><!-- .comment-body -->

          </article><!-- #comment-<?php comment_ID(); ?> -->
      <?php // End the default styling of comment
      break;
  endswitch;
}

add_action( 'after_wp_tiny_mce', 'custom_after_wp_tiny_mce' );
function custom_after_wp_tiny_mce() {
  wp_enqueue_script( 'shortcodes', get_template_directory_uri() . '/js/shortcodes.js', array(jquery), '20161215', true );
}

function wpmudev_google_analytics() { ?>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-21443852-5', 'auto');
		ga('send', 'pageview');
		
		</script>
<?php }
add_action( 'wp_head', 'wpmudev_google_analytics', 10 );

function facebook_pixel_code() { ?>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '294777347626167'); // Insert your pixel ID here.
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=294777347626167&ev=PageView&noscript=1"
	/></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->
<?php }
add_action( 'wp_head', 'facebook_pixel_code', 10 );  

if ( is_page_template( 'page-form.php' ) ) {
	function facebook_event_code() { ?>
		<!-- Facebook Event Code -->
		<script>
		fbq('track', 'ClickedApply');
		</script>
		<!-- End Facebook Event Code -->
	<?php }
	add_action( 'wp_footer', 'facebook_event_code' );  
}

/**
 * Disable admin menu on front-end
 */
add_filter('show_admin_bar', '__return_false');

/**
 * Register menus.
 */
function register_my_menus() {
  register_nav_menus(
    array(
      'primary-menu' => __( 'Primary Menu' ),
      'footer-menu-one' => __( 'Footer Column One' ),
      'footer-menu-two' => __( 'Footer Column Two' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

/**
 * Add automatic image sizes
 */
add_image_size ( 'fp_img_1920', 1920 );
add_image_size ( 'fp_img_1024', 1024 );
add_image_size ( 'fp_img_627', 767 );

/**
 * SVG Support
 */
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Override PODS meta titles
 */
add_filter('pods_meta_default_box_title','changethatname',10,5);
/* See http://codex.wordpress.org/Function_Reference/add_filter */

function changethatname($title, $pod, $fields, $type, $name ) {

  // assuming we are changing the meta box title on a pod named 'page' 
  $title = ($name=='themes') ? __('Theme Content', 'plugin_lang') : $title ;

  return $title;
}
/**
 * Stop new terms being added to "Theme Type"
 */

add_action( 'pre_insert_term', 'prevent_terms', 1, 2 );
function prevent_terms ( $term, $taxonomy ) {
  if ( 'type' === $taxonomy ) {
    return new WP_Error( 'term_addition_blocked', __( 'You cannot add terms to this taxonomy' ) );
  }
  return $term;
}

/**
 * Active menu items for archive templates
 */
add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );

function add_current_nav_class($classes, $item) {

  // Getting the current post details
  global $post;

  // Getting the post type of the current post
  $current_post_type = get_post_type_object(get_post_type($post->ID));
  $current_post_type_slug = $current_post_type->rewrite[slug];

  // Getting the URL of the menu item
  $menu_slug = strtolower(trim($item->url));

  // If the menu item URL contains the current post types slug add the current-menu-item class
  if (strpos($menu_slug,$current_post_type_slug) !== false) {

     $classes[] = 'current-menu-item';

  }

  // Return the corrected set of classes to be added to the menu item
  return $classes;

}

/**
 * Custom admin stylesheet
 */
function admin_style() {
  wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css' );
  wp_enqueue_style('admin-styles', get_template_directory_uri().'/css/admin.css');
  
}
add_action('admin_enqueue_scripts', 'admin_style');

function enable_more_buttons($buttons) {

$buttons[] = 'fontselect';
$buttons[] = 'fontsizeselect';
$buttons[] = 'styleselect';
$buttons[] = 'backcolor';
$buttons[] = 'newdocument';
$buttons[] = 'cut';
$buttons[] = 'copy';
$buttons[] = 'charmap';
$buttons[] = 'hr';
$buttons[] = 'visualaid';

return $buttons;
  
}
add_filter('mce_buttons_3', 'enable_more_buttons');

add_filter( 'tiny_mce_before_init', 'myformatTinyMCE' );
function myformatTinyMCE( $in ) {

$in['wordpress_adv_hidden'] = FALSE;

return $in;
}

function html5_insert_image($html, $id, $caption, $title, $align, $url, $size, $alt ) {
  //Always return an image with a <figure> tag, regardless of link or caption

  //Grab the image tag
  $image_tag = get_image_tag($id, '', $title, $align, $size);

  //Let's see if this contains a link
  $linkptrn = "/<a[^>]*>/";
  $found = preg_match($linkptrn, $html, $a_elem);

  // If no link, do nothing
  if($found > 0) {
    $a_elem = $a_elem[0];

    if(strstr($a_elem, "class=\"") !== false){ // If link already has class defined inject it to attribute
        $a_elem = str_replace("class=\"", "class=\"colorbox ", $a_elem);
    } else { // If no class defined, just add class attribute
        $a_elem = str_replace("<a ", "<a class=\"colorbox\" ", $a_elem);
    }
  } else {
    $a_elem = "";
  }
  // Set up the attributes for the caption <figure>
  $attributes  = (!empty($id) ? ' id="attachment_' . esc_attr($id) . '"' : '' );
  $attributes .= ' class="thumbnail wp-caption ' . 'align'.esc_attr($align) . '"';
  $output  = '<figure' . $attributes .'>';
  //add the image back in
  $output .= $a_elem;
  $output .= $image_tag;
  if($a_elem != "") {
    $output .= '</a>';
  }
  
  if ($caption) {
    $output .= '<figcaption class="caption wp-caption-text">'.$caption.'</figcaption>';
  }
  $output .= '</figure>';
  return $output;
}
add_filter('image_send_to_editor', 'html5_insert_image', 10, 9);
add_filter( 'disable_captions', create_function('$a', 'return true;') );