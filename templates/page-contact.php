<?php
/*
Template name: Contact Page
*/

get_header(); ?>

<div class="main" role="main">

  <div class="page contact-page">
    <div class="container">
      <div class="page-header">
        <div class="head-block">
          <h1 class="title">Contact us</h1>
          <span class="sub-title">How can we help you?</span>
        </div>
        <div class="bg-holder"></div>
      </div>
      <?php echo do_shortcode( '[ninja_form id=9]' ); ?>
    </div>
  </div>
  
</div>
<?php get_footer(); ?>