<?php
/*
Template name: Thank You Page
*/

get_header(); ?>

<div class="main subscribe-success thank-you" role="main">
  <div class="page">
    <div class="container c740">
      <h1 class="page-title">Thank You for Signing Up for Email Updates!</h1>
      <p class="page-content">You're all set. The next time we publish a <span class="greenish">WebsiteLearners</span> video you should get it in your email inbox.</p>
      <p class="page-content">Until then, you can follow us here:</p>
      <div class="follow">
        <a href="https://twitter.com/websitelearners" class="twitter-follow-button" data-show-count="false">Follow @websitelearners</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        <div class="fb-like" data-href="https://www.facebook.com/websitelearners" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
      </div>
      <p class="page-content soon">See you soon,</p>
      <p class="page-content"><span class="greenish">WebsiteLearners Team</span></p>
    </div>
  </div>
</div>

<?php
get_footer(); ?>