<?php
/*
Template name: Home Page
*/

get_header(); ?>

<div class="main" role="main">
  <?php 

    //ACF (Advanced Custom Fields) Fields
    $img    = get_field('home_image');
  
    $title = get_field('home_title');
    $btn = get_field('home_button_text');
    $link = get_field('home_button_link');

  ?>
  <div class="page home-page">
    <div class="container">
      <section>
        <h1><?php echo $title; ?></h1>
        <a href="#" class="btn green big intro-vid"><?php echo $btn; ?></a>
      </section>
      <picture class="home-image">
        <img src="<?php echo $img; ?>" />
      </picture>
    </div>
  </div>
</div>
<?php get_footer(); ?>