<?php
/*
Template name: About Page
*/

get_header(); ?>

<div class="main" role="main">
  <div class="page">
    <div class="container c740">
      <div class="about">
        <h1 class="page-title"><?php the_title(); ?></h1>
        <?php 
          $pageid = get_the_id();
          $content_post = get_post($pageid);
          $content = $content_post->post_content;
          $content = apply_filters('the_content', $content);
          $content = str_replace(']]>', ']]&gt;', $content);
          echo $content;
        ?>
      </div>
      <?php 
        $subscribe_title    = get_field('subscribe_block_title');
        $subscribe_subtitle = get_field('subscribe_block_subtitle');
        $subscribe_list_id  = get_field('subscribe_block_list_id');

        if($subscribe_title) : 
      ?>
      <div class="subscribe-wrap">
        <div class="subscribe-title"><?php echo $subscribe_title ?></div>
        <?php if($subscribe_subtitle) : ?>
        <div class="subscribe-subtitle"><?php echo $subscribe_subtitle ?></div>
        <?php endif; ?>
        <form method="post" class="af-form-wrapper subscribe" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" target="_blank" >
          <div style="display: none;">
          <input type="hidden" name="meta_web_form_id" value="635567117" />
          <input type="hidden" name="meta_split_id" value="" />
          <input type="hidden" name="listname" value="awlist<?php echo $subscribe_list_id ?>" />
          <input type="hidden" name="redirect" value="https://websitelearners.com/please-confirm-your-email/" id="redirect_ab439d9b075db7596cd59387c49bfc0a" />
          <input type="hidden" name="meta_adtracking" value="resource" />
          <input type="hidden" name="meta_message" value="1" />
          <input type="hidden" name="meta_required" value="email" />
          <input type="hidden" name="meta_tooltip" value="" />
          </div>

          <input class="text" id="awf_field-86169729" type="text" name="email" placeholder="Your Email Address" value="" tabindex="500" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
          <input name="submit" class="submit btn green medium" type="submit" value="Subscribe" tabindex="501" />
          <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=bMysrGzsjIzs" alt="" /></div>
        </form>
      </div>
      <?php endif; ?>
    </div>
  </div>
</div>

<?php
get_footer(); ?>