<?php
/*
Template name: Application Form Page
*/

get_header(); ?>

<div class="main apply" role="main">
  <div class="form-wrap">
    <?php
      $form_id = get_field('form_id');
    ?>
    <iframe id="typeform-full" width="100%" height="100%" frameborder="0" src="https://websitelearners.typeform.com/to/<?php echo $form_id; ?>"></iframe>
  </div>
</div>

<?php
get_footer(); ?>
