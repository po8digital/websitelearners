<?php
/*
Template name: Resource Page
*/

get_header(); ?>

<div class="main" role="main">

  <div class="page">
    <?php if( have_rows('resource_builder') ): ?>

      <?php while ( have_rows('resource_builder') ) : the_row(); ?>


      <?php if( get_row_layout() == 'step_one' ):  ?>
    
        <?php 
          $id = get_the_ID();
    
          $page_title = get_field('page_title', $id);
          $checker    = get_sub_field('shortcode');
          $title      = get_sub_field('step_title');
          $sub_title  = get_sub_field('step_sub_title');
          $show       = get_sub_field('domain_checker');
          $img        = get_sub_field('step_image');
          $border       = get_sub_field('bottom_border');

          //Lower case everything
          $string = strtolower($title);
          //Make alphanumeric (removes all other characters)
          $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
          //Clean up multiple dashes or whitespaces
          $string = preg_replace("/[\s-]+/", " ", $string);
          //Convert whitespaces and underscore to dash
          $string = preg_replace("/[\s_]/", "-", $string);

        ?>

        <section class="step <?php if($border) { ?>no-border<?php } ?>" id="<?php echo $string; ?>">
          <div class="wrap in blocked">
            <div class="content-left">
              <span class="page-title"><?php echo $page_title ?></span>
              <h1 class="title"><?php echo $title ?></h1>
              <span class="sub-title"><?php echo $sub_title ?></span>

              <?php if ($show) { echo do_shortcode( ''.$checker.'' ); } ?>
            </div>
            <div class="content-right">
              <img src="<?php echo $img; ?>" title="<?php echo $title; ?>" alt="<?php echo $title; ?>" />
            </div>
          </div>
        </section>

      <?php elseif( get_row_layout() == 'step_two' ): ?>

        <?php 
    
          $title        = get_sub_field('step_title');
          $sub_title    = get_sub_field('step_sub_title');
          $content      = get_sub_field('step_content');
          $button_text  = get_sub_field('step_button_text');
          $button_link  = get_sub_field('step_button_link');
          $bg           = get_sub_field('background_image');
          $border       = get_sub_field('bottom_border');
    
          //Lower case everything
          $string = strtolower($title);
          //Make alphanumeric (removes all other characters)
          $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
          //Clean up multiple dashes or whitespaces
          $string = preg_replace("/[\s-]+/", " ", $string);
          //Convert whitespaces and underscore to dash
          $string = preg_replace("/[\s_]/", "-", $string);

        ?>
    
        <section class="step step-two <?php if($border) { ?>no-border<?php } ?>" id="<?php echo $string; ?>">
          <div class="wrap in has-bg" style="background-image: url('<?php echo $bg ?>');">
            <div class="step-content centered">
              <h1 class="title"><?php echo $title ?></h1>
              <span class="sub-title"><?php echo $sub_title ?></span>
              <p><?php echo $content ?></p>
              <a class="btn big green" href="<?php echo $button_link; ?>" target="_blank"><?php echo $button_text; ?></a>
            </div>
          </div>
        </section>
      

      <?php elseif( get_row_layout() == 'step_three' ): ?>

        <?php 
    
          $title        = get_sub_field('step_title');
          $sub_title    = get_sub_field('step_sub_title');
          $content      = get_sub_field('step_content');
          $button_text  = get_sub_field('step_button_text');
          $button_link  = get_sub_field('step_button_link');
          $bg           = get_sub_field('background_image');
          $border       = get_sub_field('bottom_border');
    
          //Lower case everything
          $string = strtolower($title);
          //Make alphanumeric (removes all other characters)
          $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
          //Clean up multiple dashes or whitespaces
          $string = preg_replace("/[\s-]+/", " ", $string);
          //Convert whitespaces and underscore to dash
          $string = preg_replace("/[\s_]/", "-", $string);

        ?>
    
        <section class="step <?php if($border) { ?>no-border<?php } ?>" id="<?php echo $string; ?>">
          <div class="wrap out no-padding-side">
            <div class="step-content centered">
              <h1 class="title"><?php echo $title ?></h1>
              <span class="sub-title"><?php echo $sub_title ?></span>
              
              <div class="theme-slider">
                <div class="slider-wrap">
                  <?php 
                    $itms  = get_sub_field("featured_items"); 
                    $limit = get_sub_field("limited"); 

                    global $wpdb; 
                
                    $params = array(
                      'where' => array( 'ID' => wp_list_pluck($itms, 'ID')),
                      'limit' => $limit, 
                      'orderby' => 'menu_order'
                      );
                  ?>
                  <?php $slides = pods( 'themes', $params); ?>
                  <?php if ( $slides->total() > 0 ) { ?>
                  <!-- Small Slider -->
                  <div class="small-slider">
                    <div class="owl-carousel slider-inner">
                      <?php while ($slides->fetch() ) { ?>
                      <?php 
                        $id     = $slides->field('id');
                        $name   = $slides->field('theme_name');
                        //ACF (Advanced Custom Fields) Fields
                        $iphone = get_field('mobile', $id);
                      ?>
                      
                      <div class="slide-item">
                        <div class="inner">
                          <picture>
                            <img itemprop="image" src="<?php if ($iphone){ echo $iphone; } ?>" alt="<?php echo $name; ?>" title="<?php echo $name; ?>" />
                          </picture>
                        </div>                      
                      </div>
                      
                      <?php } ?>
                    </div>
                    <img class="phone visible" src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/phone.svg" alt="">
                  </div>
                  <?php } ?>
                  <?php $themes  = pods( 'themes', $params); ?>  
                  <?php if ( $themes->total() > 0 ) { ?>  
                  <!-- Main Slider -->
                  <div class="main-slider">
                    <div class="owl-carousel slider-inner">
                      <?php while ($themes->fetch() ) { ?>
                      <?php 
                        //PODS Fields
                        $id         = $themes->field('id');
                        $name       = $themes->field('theme_name');
                        $info       = $themes->field('theme_info');
                        $demo       = $themes->field('demo_link');
                        $guide      = $themes->field('setup_guide');
                        $official   = $themes->field('theme_link');
                        $file       = wp_get_attachment_url( $themes->field('theme_file.ID') );

                        //ACF (Advanced Custom Fields) Fields
                        $imgID    = get_field('featured_image', $id);
                        $desktop   = "fp_img_1920";
                        $tablet    = "fp_img_1024";
                        $mobile    = "fp_img_627";

                        $desktop_url   = wp_get_attachment_image_src( $imgID, $desktop );
                        $tablet_url    = wp_get_attachment_image_src( $imgID, $tablet );
                        $mobile_url    = wp_get_attachment_image_src( $imgID, $mobile );

                        //Get related terms
                        $terms = get_the_terms( $id, 'type' ); 
                        
                      ?>
                      
                      <div class="slide-item">
                        <div class="inner">
                          <div class="item-type"><div class="type"><?php if ($terms[0]->name === 'Premium') { echo $terms[0]->name; } ?></div></div>
                          <span class="dots"></span>
                          <picture>
                            <img
                             itemprop="image"
                             srcset="<?php if ($desktop_url){ echo $desktop_url[0]; } ?> 1920w,
                                     <?php if ($tablet_url){ echo $tablet_url[0]; } ?> 1366w,
                                     <?php if ($mobile_url){ echo $mobile_url; } ?> 627w"
                             src="<?php echo $desktop_url[0]; ?>"
                             alt="<?php echo $name; ?>" title="<?php echo $name; ?>" />
                            
                          </picture>
                           <div class="view-theme">
                            <a href="<?php if($demo) { echo $demo; } ?>" <?php if($demo) {echo 'target="_blank"'; } ?>class="viewer">View Theme <i class="caret has-download"></i></a>
                            <div class="operator">
                              <a href="<?php if($file) { echo $file; } else if($official) { echo $official; } ?>" class="download"><?php if($file) {?>Download Theme<?php } else if($official) { ?>Purchase Theme<?php } ?></a>
                            </div>
                          </div>
                        </div>    
                      </div>
                      
                      <?php } ?>

                    </div>
                  </div>
                  <?php } ?>
                  <!-- Slide Nav -->
                  <div class="slide-nav">
                    <div class="prev-slide">
                      <a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a> 
                    </div>
                    <div class="next-slide">
                      <a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a> 
                    </div>
                  </div>
                </div>
              </div> 
              <div class="link-through">
                <a href="/themes#free" target="_blank">See all free themes</a>
                <a href="/themes#premium" target="_blank">See all premium themes</a>
              </div>
              
            </div>
          </div>
        </section>

      <?php elseif( get_row_layout() == 'step_four' ): ?>

        <?php 
    
          $title        = get_sub_field('step_title');
          $sub_title    = get_sub_field('step_sub_title');
          $content      = get_sub_field('step_content');
          $button_text  = get_sub_field('step_button_text');
          $button_link  = get_sub_field('step_button_link');
          $bg           = get_sub_field('background_image');
          $border       = get_sub_field('bottom_border');
    
          //Lower case everything
          $string = strtolower($title);
          //Make alphanumeric (removes all other characters)
          $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
          //Clean up multiple dashes or whitespaces
          $string = preg_replace("/[\s-]+/", " ", $string);
          //Convert whitespaces and underscore to dash
          $string = preg_replace("/[\s_]/", "-", $string);

        ?>
    
        <section class="step add-content <?php if($border) { ?>no-border<?php } ?>" id="<?php echo $string; ?>">
          <div class="wrap normal" style="background-image: url('<?php echo $bg ?>');">
            <div class="step-content">
              <h1 class="title centered"><?php echo $title ?></h1>
              <span class="sub-title centered"><?php echo $sub_title ?></span>
              
              <?php if( have_rows('content_steps') ): ?>
              <div class="content-steps">
                <?php $i = 1; while ( have_rows('content_steps') ) : the_row(); ?>

                <?php 
                  $image   = get_sub_field('step_image');
                  $title   = get_sub_field('step_title');
                  $content = get_sub_field('step_content');
                ?>

                <article class="content-step">
                  <h1><span><?php echo $i; ?>.</span> <?php echo $title; ?></h1>
                  <div class="display"><?php echo $content; ?></div>
                  <picture>
                    <img src="<?php echo $image; ?>" alt="<?php echo $title; ?>" />
                  </picture>
                </article>

                <?php $i++; endwhile; ?>
              </div>
              <?php endif; ?>
            </div>
          </div>
        </section>
    
      <?php elseif( get_row_layout() == 'step_end' ): ?>

        <?php 
    
          $title        = get_sub_field('step_title');
          $sub_title    = get_sub_field('step_sub_title');
          $content      = get_sub_field('step_content');

          $image        = get_sub_field('step_image');
          $margin       = get_sub_field('img_margin');
          $bg_left      = get_sub_field('step_background_left');
          $bg_right     = get_sub_field('step_background_right');
          $border       = get_sub_field('bottom_border');

        ?>
    
        <section class="step end <?php if($border) { ?>no-border<?php } ?>">
          <div class="wrap out no-padding">
            <div class="step-content centered">
              <div class="bg-wrap-left" style="background-image: url('<?php echo $bg_left ?>');"></div>
              <div class="content-wrap">
                <h1 class="title white"><?php echo $title ?></h1>
                <span class="sub-title"><?php echo $sub_title ?></span>
                <div class="img-hold" style="margin-bottom: <?php echo $margin; ?>px;">
                  <img src="<?php echo $image; ?>" />
                </div>
                <p><?php echo $content; ?></p>
              </div>
              <div class="bg-wrap-right"  style="background-image: url('<?php echo $bg_right ?>');"></div>
            </div>
          </div>
        </section>
    
      <?php elseif( get_row_layout() == 'support_section' ): ?>

        <?php 
    
          $title        = get_sub_field('step_title');
          $sub_title    = get_sub_field('step_sub_title');
          $ninja        = get_sub_field('ninja_form_shortcode');
          $border       = get_sub_field('bottom_border');
        ?>
    
        <section class="step support-section centered<?php if($border) { ?> no-border<?php } ?>">
          <div class="wrap in">
            <div class="step-content">
              <div class="content-wrap">
                <h1 class="title"><?php echo $title ?></h1>
                <span class="sub-title"><?php echo $sub_title ?></span>
                <div class="support">
                  <h3>Contact us using the form below</h3>
                  <div class="support-wrap">
                    <?php if ($ninja) { echo do_shortcode( ''.$ninja.'' ); } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    
      <?php elseif( get_row_layout() == 'subscribe_block' ): ?>

        <?php 
          $title        = get_sub_field('subscribe_heading');
          $sub_title    = get_sub_field('subscribe_content');
          $optin        = get_sub_field('subscribe_shortcode');
        ?>
    
        <section class="subscribe-section">
          <div id="subscribe-block" class="subscribe-block">
            <div class="inner">
              <div class="upper">
                <h1><?php echo $title; ?></h1>
                <h3><?php echo $sub_title; ?></h3>
              </div>
              <div class="lower">

                <form method="post" class="af-form-wrapper subscribe" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" target="_blank" >
                  <div style="display: none;">
                  <input type="hidden" name="meta_web_form_id" value="635567117" />
                  <input type="hidden" name="meta_split_id" value="" />
                  <input type="hidden" name="listname" value="awlist4443488" />
                  <input type="hidden" name="redirect" value="https://websitelearners.com/please-confirm-your-email/" id="redirect_ab439d9b075db7596cd59387c49bfc0a" />
                  <input type="hidden" name="meta_adtracking" value="resource" />
                  <input type="hidden" name="meta_message" value="1" />
                  <input type="hidden" name="meta_required" value="email" />
                  <input type="hidden" name="meta_tooltip" value="" />
                  </div>

                  <input class="text" id="awf_field-86169729" type="text" name="email" placeholder="Your Email Address" value="" tabindex="500" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                  <input name="submit" class="submit btn green medium" type="submit" value="Submit" tabindex="501" />
                  <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=bMysrGzsjIzs" alt="" /></div>
                </form>
              </div>
            </div>
          </div>
        </section>

      <?php endif; ?>

      <?php endwhile; ?>

      <?php else : ?>

      <?php // no layouts found ?>

    <?php endif; ?>

    <?php if( have_rows('whats_next') ): ?>
    <section class="whats-next-section">
      <div class="whats-next">
        <h1 class="title"><?php echo the_field('what_section_title'); ?></h1>
        <?php while ( have_rows('whats_next') ) : the_row(); ?>
        <?php 
          $image = get_sub_field('what_image');
          $title = get_sub_field('what_title');
          $link  = get_sub_field('what_link');
        ?>
        <article class="next-item">
          <a href="<?php echo $link; ?>">
            <picture class="item-image">
              <img src="<?php echo $image; ?>" alt="<?php echo $title; ?>" />
            </picture>
            <h1 class="item-title"><?php echo $title; ?></h1>
          </a>
        </article>
        <?php endwhile; ?>
      </div>
    </section>
    <?php else : ?>
    
    <?php endif; ?>
  </div>
  
</div>
<?php get_footer(); ?>