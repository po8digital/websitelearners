<?php
/*
Template name: Standard Page
*/

get_header(); ?>

<div class="main" role="main">
  <div class="page">
    <div class="container c740">
      <div class="about standard">
        <h1 class="page-title"><?php the_title(); ?></h1>
        <?php 
          $pageid = get_the_id();
          $content_post = get_post($pageid);
          $content = $content_post->post_content;
          $content = apply_filters('the_content', $content);
          $content = str_replace(']]>', ']]&gt;', $content);
          echo $content;
        ?>
      </div>
      <?php 
        $subscribe_title    = get_field('subscribe_block_title');
        $subscribe_subtitle = get_field('subscribe_block_subtitle');
        $subscribe_list_id  = get_field('subscribe_block_list_id');

        if($subscribe_title) : 
      ?>
      <div class="apply-wrap">

      </div>
      <?php endif; ?>
    </div>
  </div>
</div>

<?php
get_footer(); ?>