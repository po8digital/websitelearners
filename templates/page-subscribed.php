<?php
/*
Template name: Subscribed Page
*/

get_header(); ?>

<div class="main subscribe-success" role="main">
  <div class="page">
    <div class="container c740">
      <p class="page-content">Just one more step...</p>
      <h1 class="page-title">Confirm your email</h1>
      <i class="fa fa-check-circle" aria-hidden="true"></i>
      <p class="page-content">Check your email inbox and follow the instructions in<br> the message we sent you.</p>
      <a class="btn medium green" href="https://mail.google.com/mail/u/0/#search/contact%40websitelearners.com%20Please%20Confirm%20your%20Email">Go to Gmail</a>
    </div>
  </div>
</div>

<?php
get_footer(); ?>