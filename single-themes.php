<?php 
  global $wpdb; 

  $ID = get_the_ID();
  $current_post = get_queried_object();
  //Get related terms
  $term_list = wp_get_post_terms($ID, 'type', array("fields" => "all"));
  $type = $term_list[0]->term_id;

  $params = array( 
    'where' => 'ID = '.$ID
  );
  $theme = pods ( 'themes', $params );
  if( $theme->total() ) {
    while ( $theme->fetch() ) :
      
      $id         = $theme->field('id');
      $slug       = $theme->field('slug');
      $name       = $theme->field('theme_name');
      $info       = $theme->field('theme_info');
      $demo       = $theme->field('demo_link');
      $fallback   = $theme->field('demo_link_fallback');
      $guide      = $theme->field('setup_guide');
      $official   = $theme->field('theme_link');
      $purchase   = $theme->field('purchase_link');
      $file       = wp_get_attachment_url( $theme->field('theme_file.ID') );
    
      $rtext      = $theme->field('resource_page_text');
      $rlink      = $theme->field('resource_page_link');
    
      //ACF (Advanced Custom Fields) Fields
      $imgID     = get_field('featured_image', $id);
      $desktop   = "fp_img_1920";
      $tablet    = "fp_img_1024";
      $mobile    = "fp_img_627";

      $desktop_uri   = wp_get_attachment_image_src( $imgID, $desktop );
      $tablet_uri    = wp_get_attachment_image_src( $imgID, $tablet );
      $mobile_uri    = wp_get_attachment_image_src( $imgID, $mobile );
        
    endwhile;
  }
?>

<div class="preview-window">
  <div class="scroll-tip"><i class="icon-mouse"></i><p>Scroll mouse to view next theme or use the arrow keys</p></div>
  <div class="preview-inner modal-carousel modal-carousel-main modal-carousel-outer">
    
    <div class="preview item-<?php echo $id; ?>">
      <img itemprop="image" src="<?php if ($desktop_uri){ echo $desktop_uri[0]; } ?>" alt="<?php echo $name; ?>" title="<?php echo $name; ?>" />
    </div>

    <?php 
      $list = array(
        'orderby' => 'menu_order',
        'order' => 'DESC',
        'limit' => -1
      );
      $mainSlide = pods( 'themes', $list); 
    ?>

    <?php if ( $mainSlide->total() > 0 ) { ?>
      <?php while ($mainSlide->fetch() ) { ?>
      <?php 
        //PODS Fields

        $podID     = $mainSlide->field('id');

        //ACF (Advanced Custom Fields) Fields
        $imgID      = get_field('featured_image', $podID);
        $desktop   = "fp_img_1920";
        $tablet    = "fp_img_1024";
        $mobile    = "fp_img_627";

        $desktop_url   = wp_get_attachment_image_src( $imgID, $desktop );
        $tablet_url    = wp_get_attachment_image_src( $imgID, $tablet );
        $mobile_url    = wp_get_attachment_image_src( $imgID, $mobile );

      ?>
      <?php if ($podID !== $ID) { ?>
      <div class="preview item-<?php echo $podID; ?>">
        <img itemprop="image" src="<?php if ($desktop_url){ echo $desktop_url[0]; } ?>" alt="<?php echo $name; ?>" title="<?php echo $name; ?>" />
      </div>
      <?php } ?>
    
      <?php } ?>
    <?php } ?>   
  </div>
</div>
<div class="carousel-column modal-carousel modal-carousel-vertical">
  
  <div class="nav-item item-<?php echo $id; ?>">
    <picture>
      <img itemprop="image" src="<?php if ($mobile_uri){ echo $mobile_uri[0]; } ?>" alt="<?php echo $name; ?>" title="<?php echo $name; ?>" />
    </picture>
  </div>
  
  <?php $pods = pods( 'themes', $list); ?>

  <?php if ( $pods->total() > 0 ) { ?>
    <?php while ($pods->fetch() ) { ?>
    <?php 
      //PODS Fields

      $podID     = $pods->field('id');

      //ACF (Advanced Custom Fields) Fields
      $imgID    = get_field('featured_image', $podID);
      $desktop   = "fp_img_1920";
      $tablet    = "fp_img_1024";
      $mobile    = "fp_img_627";

      $desktop_url   = wp_get_attachment_image_src( $imgID, $desktop );
      $tablet_url    = wp_get_attachment_image_src( $imgID, $tablet );
      $mobile_url    = wp_get_attachment_image_src( $imgID, $mobile );

    ?>
  
    <?php if ($podID !== $ID) { ?>
    <div class="nav-item item-<?php echo $podID; ?>">
      <picture>
        <img itemprop="image" src="<?php if ($mobile_url){ echo $mobile_url[0]; } ?>" alt="<?php echo $name; ?>" title="<?php echo $name; ?>" />
      </picture>
    </div>
    <?php } ?>
  
    <?php } ?>
  <?php } ?> 
  
</div>

<div class="theme-info">
  <button class="close-modal"></button>

  <div class="theme-contents modal-carousel content-carousel modal-carousel-outer">
  
    <article class="theme-content item-<?php echo $id; ?>">
      <h1 class="theme-title"><?php echo $name; ?></h1>
      <p class="about-theme"><?php echo $info; ?></p>
      <div class="theme-links">
        <a href="<?php if($file) { echo $file; } else if($purchase) { echo $purchase; } ?>" class="download btn medium green"><?php if($file) {?>Download<?php } else if($purchase) { ?>Purchase<?php } ?></a>
        <a <?php if($demo){?>href="<?php echo $demo; ?>"<?php } ?> class="<?php if($demo){ ?>demo <?php }?> anim-link" target="_blank">Live Demo</a>
      </div>
      <div class="meta-links">
       <?php if($rtext) {?><a href="<?php echo $rlink; ?>" class="anim-link" target="_blank"><?php echo $rtext; ?></a><?php } ?>
       <?php if ($guide) { ?><a href="<?php echo $guide; ?>" class="anim-link" target="_blank">Theme Setup Guide</a><?php } ?>
        <a href="<?php echo $official; ?>" class="anim-link" target="_blank">View Official Theme Page</a>
      </div>
    </article>
    
    <?php $cont = pods( 'themes', $list); ?>
    <?php if ( $cont->total() > 0 ) { ?>
      <?php while ($cont->fetch() ) { ?>
      <?php 
        //PODS Fields

        $podID     =  $cont->field('id');
        $slug       = $cont->field('slug');
        $name       = $cont->field('theme_name');
        $info       = $cont->field('theme_info');
        $demo       = $cont->field('demo_link');
        $fallback   = $cont->field('demo_link_fallback');
        $guide      = $cont->field('setup_guide');
        $official   = $cont->field('theme_link');
        $purchase   = $cont->field('purchase_link');
        $file       = wp_get_attachment_url( $cont->field('theme_file.ID') );
  
        $rtext      = $cont->field('resource_page_text');
        $rlink      = $cont->field('resource_page_link');
      ?>
    
      <?php if ($podID !== $ID) { ?>
      <article class="theme-content item-<?php echo $podID; ?>">
        <h1 class="theme-title"><?php echo $name; ?></h1>
        <p class="about-theme"><?php echo $info; ?></p>
        <div class="theme-links">
          <a href="<?php if($file) { echo $file; } else if($purchase) { echo $purchase; } ?>" class="download btn medium green"><?php if($file) {?>Download<?php } else if($purchase) { ?>Purchase<?php } ?></a>
          <a <?php if($demo){?>href="<?php echo $demo; ?>"<?php } ?> class="<?php if($demo){ ?>demo <?php }?> anim-link" target="_blank">Live Demo</a>
        </div>
        <div class="meta-links">
          <?php if($rtext) {?><a href="<?php echo $rlink; ?>" class="anim-link" target="_blank"><?php echo $rtext; ?></a><?php } ?>
          <?php if ($guide) { ?><a href="<?php echo $guide; ?>" class="anim-link" target="_blank">Theme Setup Guide</a><?php } ?>
          <a href="<?php echo $official; ?>" class="anim-link" target="_blank">View Official Theme Page</a>
        </div>
      </article>
      <?php } ?>
    
      <?php } ?>
    <?php } ?> 
  </div>
</div>


